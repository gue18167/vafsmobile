﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafsmobile.api.Model
{
    public partial class n_note
    {
        [Key]
        [Column(TypeName = "int(11)")]
        public int n_id { get; set; }
        [Required]
        [Column(TypeName = "text")]
        public string n_description { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime n_createdate { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime n_deadline { get; set; }
        [Column(TypeName = "int(11)")]
        public int? n_u_user { get; set; }
        [Column(TypeName = "int(11)")]
        public int? n_g_group { get; set; }
        [Column(TypeName = "varchar(45)")]
        public string n_title { get; set; }
        [Column(TypeName = "int(11)")]
        public int? n_c_calendarinformation { get; set; }

        [ForeignKey(nameof(n_c_calendarinformation))]
        [InverseProperty(nameof(c_calendarinformation.n_note))]
        public virtual c_calendarinformation n_c_calendarinformationNavigation { get; set; }
        [ForeignKey(nameof(n_g_group))]
        [InverseProperty(nameof(g_group.n_note))]
        public virtual g_group n_g_groupNavigation { get; set; }
        [ForeignKey(nameof(n_u_user))]
        [InverseProperty(nameof(u_user.n_note))]
        public virtual u_user n_u_userNavigation { get; set; }
    }
}
