﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace vafsmobile.api.Model
{
    public partial class vafsDbContext : DbContext
    {

        public vafsDbContext(DbContextOptions<vafsDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<c_calendarinformation> c_calendarinformation { get; set; }
        public virtual DbSet<g_group> g_group { get; set; }
        public virtual DbSet<gs_groupstatus> gs_groupstatus { get; set; }
        public virtual DbSet<i_invitation> i_invitation { get; set; }
        public virtual DbSet<n_note> n_note { get; set; }
        public virtual DbSet<r_role> r_role { get; set; }
        public virtual DbSet<u_g_groupmember> u_g_groupmember { get; set; }
        public virtual DbSet<u_user> u_user { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseMySql("server=192.189.51.11;database=vafsDb;user=management;password=vafspassword;treattinyasboolean=true", x => x.ServerVersion("5.7.28-mysql"));
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<c_calendarinformation>(entity =>
            {
                entity.HasKey(e => e.c_id)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.c_g_group)
                    .HasName("c_g_group");

                entity.HasIndex(e => e.c_u_user)
                    .HasName("c_u_user");

                entity.Property(e => e.c_createdate)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.c_deadline).HasDefaultValueSql("'0000-00-00 00:00:00.0'");

                entity.HasOne(d => d.c_g_groupNavigation)
                    .WithMany(p => p.c_calendarinformation)
                    .HasForeignKey(d => d.c_g_group)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("c_calendarinformation_ibfk_2");

                entity.HasOne(d => d.c_u_userNavigation)
                    .WithMany(p => p.c_calendarinformation)
                    .HasForeignKey(d => d.c_u_user)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("c_calendarinformation_ibfk_1");
            });

            modelBuilder.Entity<g_group>(entity =>
            {
                entity.HasKey(e => e.g_id)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.g_gs_groupstatus)
                    .HasName("g_gs_groupstatus");

                entity.Property(e => e.g_key)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.g_name)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.g_gs_groupstatusNavigation)
                    .WithMany(p => p.g_group)
                    .HasForeignKey(d => d.g_gs_groupstatus)
                    .HasConstraintName("g_group_ibfk_1");
            });

            modelBuilder.Entity<gs_groupstatus>(entity =>
            {
                entity.HasKey(e => e.gs_id)
                    .HasName("PRIMARY");

                entity.Property(e => e.gs_bezeichnung)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<i_invitation>(entity =>
            {
                entity.HasKey(e => new { e.i_u_user, e.i_g_group })
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.i_g_group)
                    .HasName("i_g_group");

                entity.Property(e => e.i_expiredate)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.i_g_groupNavigation)
                    .WithMany(p => p.i_invitation)
                    .HasForeignKey(d => d.i_g_group)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("i_invitation_ibfk_2");

                entity.HasOne(d => d.i_u_userNavigation)
                    .WithMany(p => p.i_invitation)
                    .HasForeignKey(d => d.i_u_user)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("i_invitation_ibfk_1");
            });

            modelBuilder.Entity<n_note>(entity =>
            {
                entity.HasKey(e => e.n_id)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.n_c_calendarinformation)
                    .HasName("n_c_calendarinformation");

                entity.HasIndex(e => e.n_g_group)
                    .HasName("n_g_group");

                entity.HasIndex(e => e.n_u_user)
                    .HasName("n_u_user");

                entity.Property(e => e.n_createdate)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.n_deadline).HasDefaultValueSql("'0000-00-00 00:00:00'");

                entity.Property(e => e.n_description)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.n_title)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.HasOne(d => d.n_c_calendarinformationNavigation)
                    .WithMany(p => p.n_note)
                    .HasForeignKey(d => d.n_c_calendarinformation)
                    .HasConstraintName("n_note_ibfk_3");

                entity.HasOne(d => d.n_g_groupNavigation)
                    .WithMany(p => p.n_note)
                    .HasForeignKey(d => d.n_g_group)
                    .HasConstraintName("n_note_ibfk_2");

                entity.HasOne(d => d.n_u_userNavigation)
                    .WithMany(p => p.n_note)
                    .HasForeignKey(d => d.n_u_user)
                    .HasConstraintName("n_note_ibfk_1");
            });

            modelBuilder.Entity<r_role>(entity =>
            {
                entity.HasKey(e => e.r_id)
                    .HasName("PRIMARY");

                entity.Property(e => e.r_description)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<u_g_groupmember>(entity =>
            {
                entity.HasKey(e => new { e.u_g_user, e.u_g_group })
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.u_g_group)
                    .HasName("u_g_group");

                entity.HasIndex(e => e.u_g_role)
                    .HasName("u_g_role");

                entity.Property(e => e.u_g_membersince)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.u_g_groupNavigation)
                    .WithMany(p => p.u_g_groupmember)
                    .HasForeignKey(d => d.u_g_group)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("u_g_groupmember_ibfk_2");

                entity.HasOne(d => d.u_g_roleNavigation)
                    .WithMany(p => p.u_g_groupmember)
                    .HasForeignKey(d => d.u_g_role)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("u_g_groupmember_ibfk_3");

                entity.HasOne(d => d.u_g_userNavigation)
                    .WithMany(p => p.u_g_groupmember)
                    .HasForeignKey(d => d.u_g_user)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("u_g_groupmember_ibfk_1");
            });

            modelBuilder.Entity<u_user>(entity =>
            {
                entity.HasKey(e => e.u_id)
                    .HasName("PRIMARY");

                entity.Property(e => e.u_email)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.u_password)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.u_registerdate)
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.u_username)
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
