﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafsmobile.api.Model
{
    public partial class u_user
    {
        public u_user()
        {
            c_calendarinformation = new HashSet<c_calendarinformation>();
            i_invitation = new HashSet<i_invitation>();
            n_note = new HashSet<n_note>();
            u_g_groupmember = new HashSet<u_g_groupmember>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int u_id { get; set; }
        [Required]
        [Column(TypeName = "varchar(45)")]
        public string u_username { get; set; }
        [Required]
        [Column(TypeName = "varchar(45)")]
        public string u_email { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime u_registerdate { get; set; }
        [Column(TypeName = "text")]
        public string u_password { get; set; }

        [InverseProperty("c_u_userNavigation")]
        public virtual ICollection<c_calendarinformation> c_calendarinformation { get; set; }
        [InverseProperty("i_u_userNavigation")]
        public virtual ICollection<i_invitation> i_invitation { get; set; }
        [InverseProperty("n_u_userNavigation")]
        public virtual ICollection<n_note> n_note { get; set; }
        [InverseProperty("u_g_userNavigation")]
        public virtual ICollection<u_g_groupmember> u_g_groupmember { get; set; }
    }
}
