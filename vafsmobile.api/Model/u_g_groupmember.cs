﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafsmobile.api.Model
{
    public partial class u_g_groupmember
    {
        [Key]
        [Column(TypeName = "int(11)")]
        public int u_g_user { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int u_g_group { get; set; }
        [Column(TypeName = "int(11)")]
        public int u_g_role { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime u_g_membersince { get; set; }

        [ForeignKey(nameof(u_g_group))]
        [InverseProperty(nameof(g_group.u_g_groupmember))]
        public virtual g_group u_g_groupNavigation { get; set; }
        [ForeignKey(nameof(u_g_role))]
        [InverseProperty(nameof(r_role.u_g_groupmember))]
        public virtual r_role u_g_roleNavigation { get; set; }
        [ForeignKey(nameof(u_g_user))]
        [InverseProperty(nameof(u_user.u_g_groupmember))]
        public virtual u_user u_g_userNavigation { get; set; }
    }
}
