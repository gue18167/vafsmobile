﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafsmobile.api.Model
{
    public partial class g_group
    {
        public g_group()
        {
            c_calendarinformation = new HashSet<c_calendarinformation>();
            i_invitation = new HashSet<i_invitation>();
            n_note = new HashSet<n_note>();
            u_g_groupmember = new HashSet<u_g_groupmember>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int g_id { get; set; }
        [Required]
        [Column(TypeName = "varchar(45)")]
        public string g_name { get; set; }
        [Column(TypeName = "int(11)")]
        public int g_limit { get; set; }
        [Column(TypeName = "varchar(45)")]
        public string g_key { get; set; }
        [Column(TypeName = "int(11)")]
        public int? g_gs_groupstatus { get; set; }

        [ForeignKey(nameof(g_gs_groupstatus))]
        [InverseProperty(nameof(gs_groupstatus.g_group))]
        public virtual gs_groupstatus g_gs_groupstatusNavigation { get; set; }
        [InverseProperty("c_g_groupNavigation")]
        public virtual ICollection<c_calendarinformation> c_calendarinformation { get; set; }
        [InverseProperty("i_g_groupNavigation")]
        public virtual ICollection<i_invitation> i_invitation { get; set; }
        [InverseProperty("n_g_groupNavigation")]
        public virtual ICollection<n_note> n_note { get; set; }
        [InverseProperty("u_g_groupNavigation")]
        public virtual ICollection<u_g_groupmember> u_g_groupmember { get; set; }
    }
}
