﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vafsmobile.api.Model
{
    public partial class gs_groupstatus
    {
        public gs_groupstatus()
        {
            g_group = new HashSet<g_group>();
        }

        [Key]
        [Column(TypeName = "int(11)")]
        public int gs_id { get; set; }
        [Column(TypeName = "varchar(45)")]
        public string gs_bezeichnung { get; set; }

        [InverseProperty("g_gs_groupstatusNavigation")]
        public virtual ICollection<g_group> g_group { get; set; }
    }
}
