﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using vafsmobile.api.Model;
using vafsmobile.dto;

namespace vafsmobile.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupsController : ControllerBase
    {
        vafsDbContext db;
        private readonly int? _authUserId;

        public GroupsController(vafsDbContext db, IHttpContextAccessor contextAccessor)
        {
            this.db = db;
            string username = contextAccessor.HttpContext.User.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
            _authUserId = db.u_user.SingleOrDefault(u => u.u_username == username)?.u_id;
        }
        // GET: api/Groups
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Groups/miroslav
        [HttpGet("{username}", Name = "GetGroup")]
        public IActionResult Get(string username)
        {
            string user = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            int userid = getId(username);

            if (user != username)
            {
                return Forbid();
            }

            IEnumerable<GroupDto> result = Enumerable.Empty<GroupDto>();
            if (db.u_user.Any(t => t.u_username == username))
            {
                var groups = from b in db.u_g_groupmember
                             where b.u_g_user == getId(username)
                             select b.u_g_group;

                result = from b in db.g_group
                         where !groups.Any(x => x == b.g_id) && b.g_gs_groupstatus == 1
                         select new GroupDto
                         {
                             id = b.g_id,
                             name = b.g_name,
                             limit = b.g_limit,
                             key = b.g_key,
                             groupstatus = b.g_gs_groupstatus
                         };
            }

            return Ok(result);
        }

        [HttpGet("mygroups/{username}", Name = "GetMyGroups")]
        public IActionResult GetPrivateGroups(string username)
        {
            string user = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            int userid = getId(username);

            if (user != username)
            {
                return Forbid();
            }

            IEnumerable<GroupDto> result = Enumerable.Empty<GroupDto>();
            if (db.u_user.Any(t => t.u_username == username))
            {
                var groups = from b in db.u_g_groupmember
                             where b.u_g_user == getId(username)
                             select b.u_g_group;

                result = from b in db.g_group
                         where groups.Any(x => x == b.g_id)
                         select new GroupDto
                         {
                             id = b.g_id,
                             name = b.g_name,
                             limit = b.g_limit,
                             key = b.g_key,
                             groupstatus = b.g_gs_groupstatus
                         };
            }

            return Ok(result);
        }
        
        [HttpPost]
        public ActionResult<GroupDto> Post([FromBody] GroupmemberDto groupmemberDto)
        {
            
            u_g_groupmember groupmember= new u_g_groupmember()
            {
                u_g_group = groupmemberDto.group,
                u_g_membersince = groupmemberDto.membersince,
                u_g_role = groupmemberDto.role,
                u_g_user = groupmemberDto.user
            };

            db.u_g_groupmember.Add(groupmember);
            db.SaveChanges();

            var result = db.g_group.FirstOrDefault(x => x.g_id == groupmember.u_g_group);

            GroupDto group = new GroupDto()
            {
                groupstatus = result.g_gs_groupstatus,
                id = result.g_id,
                key = result.g_key,
                limit = result.g_limit,
                name = result.g_name
            };

            return group;
        }

        [HttpPost("post")]
        public ActionResult<GroupDto> Post(GroupDto groupDto)
        {
            g_group group = new g_group()
            {
                g_gs_groupstatus = groupDto.groupstatus,
                g_key = groupDto.key,
                g_limit = groupDto.limit,
                g_name = groupDto.name,
            };

            db.g_group.Add(group);
            db.SaveChanges();

            return Ok(groupDto);
        }


        // POST: api/Groups
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        // PUT: api/Groups/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public int getId(string username)
        {
            int id = (from b in db.u_user
                      where b.u_username == username
                      select b.u_id).FirstOrDefault();

            return id;
        }
    }
}
