﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vafsmobile.api.Model;
using vafsmobile.dto;

namespace vafsmobile.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotesController : ControllerBase
    {
        vafsDbContext db;
        private readonly int? _authUserId;

        public NotesController(vafsDbContext db, IHttpContextAccessor contextAccessor)
        {
            this.db = db;
            string username = contextAccessor.HttpContext.User.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
            _authUserId = db.u_user.SingleOrDefault(u => u.u_username == username)?.u_id;
        }
        // GET: api/Notes
        [HttpGet]
        public IActionResult Get()
        {
                var allNotes = from n in db.n_note
                               select new NoteDto
                               {
                                   id = n.n_id,
                                   description = n.n_description,
                                   createdate = n.n_createdate,
                                   deadline = n.n_deadline,
                                   user = n.n_u_user,
                                   title = n.n_title
                               };
                return Ok(allNotes);
      
        }

        // GET: api/Notes/miroslav
        [HttpGet("{username}", Name = "Get")]
        public IActionResult Get(string username)
        {
            string user = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            int userid = getId(username);

            if (user != username)
            {
                return Forbid();
            }

            IEnumerable<NoteDto> result = Enumerable.Empty<NoteDto>();


            if(db.u_user.Any(t => t.u_username == username))
            {

               result = from n in db.n_note
                         where n.n_u_user == userid
                         select new NoteDto()
                         {
                             id = n.n_id,
                             description = n.n_description,
                             createdate = n.n_createdate,
                             deadline = n.n_deadline,
                             user = n.n_u_user,
                             title = n.n_title,
                             calendarinformation = null,
                             @group = n.n_g_group
                         };
            }
            return Ok(result);
        }

        // POST: api/Notes
        [HttpPost]
        public ActionResult<NoteDto> Post([FromBody] NoteDto notedto)
        {
            if (notedto.user != _authUserId)
            {
                return Forbid();
            }

            try
            {
                n_note note = new n_note()
                {
                    n_u_user = notedto.user,
                    n_title = notedto.title,
                    n_g_group = notedto.group,
                    n_description = notedto.description,
                    n_createdate = notedto.createdate,
                    n_deadline = notedto.deadline,
                    n_id = notedto.id,
                    n_c_calendarinformation = null
                };

                db.n_note.Add(note);

                db.SaveChanges();

                return Ok(new NoteDto
                {
                    user = note.n_u_user,
                    title = note.n_title,
                    group = note.n_g_group,
                    description = note.n_description,
                    createdate = note.n_createdate,
                    deadline = note.n_deadline,
                    id = note.n_id
                }) ;
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Notes/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            n_note found = db.n_note.Where(
                n => n.n_u_user == _authUserId && n.n_id == id).SingleOrDefault();

            if(found == null) { return NotFound(); }
            try
            {
                db.n_note.Remove(found);
                db.SaveChanges();
                return Ok();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        public int getId(string username)
        {
            int id = (from b in db.u_user
                      where b.u_username == username
                      select b.u_id).FirstOrDefault();

            return id;
        }
    }
}
