﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.api.Model;
using vafsmobile.dto;

namespace vafsmobile.api.Services
{
    /// <summary>
    /// Registiert neue Benutzer und verifiziert Benutzerlogins. Generiert JWT Strings bei
    /// verifizierten Benutzern.
    /// </summary>
    public class UserService
    {
        private readonly vafsDbContext db;
        private readonly IConfiguration configuration;
        /// <summary>
        /// Bestimmt die Dauer Gültigkeit des Tokens.
        /// </summary>
        public TimeSpan ExpirationTime => new TimeSpan(24, 0, 0);

        /// <summary>
        /// Konstruktor. Setzt die ASP.NET Konfiguration und den DB Context.
        /// </summary>
        /// <param name="context">Wird über Depenency Injection durch services.AddDbContext() übergeben.</param>
        /// <param name="configuration">Wird über Depenency Injection von ASP.NET übergeben.</param>
        public UserService(vafsDbContext context, IConfiguration configuration)
        {
            this.configuration = configuration;
            this.db = context;
        }

        /// <summary>
        /// Generiert den JSON Web Token für den übergebenen User, wenn dieser authentifiziert
        /// werden konnte.
        /// </summary>
        /// <param name="user">User, der verifiziert werden soll.</param>
        /// <returns>
        /// Token, wenn der User Authentifiziert werden konnte. 
        /// Null wenn der Benutzer nicht gefunden wurde.
        /// </returns>
        public UserDto GenerateToken(UserDto user)
        {
            // TODO:
            // 1. Prüfen des Usernamens
            //u_user blauser = db.u_user.Find(user.username);
            u_user dbUser = db.u_user.FirstOrDefault(x => x.u_username == user.username);
            if (dbUser == null) { return null; }

            // 2. Lesen des salt aus der DB
            // 3. Stimmt CalculateHash(user.Password, salt) mit dem Hash der DB überein?
            if (!(ComputeSha256Hash(user.password) == dbUser.u_password))
            { return null; }
            else
            { user.password = ""; }

            // 4. Die Rolle aus der DB lesen oder bestimmen.
            user.id = (from u in db.u_user
                      where u.u_username == user.username
                      select u.u_id).FirstOrDefault();

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            // Aus appsettings.json das Secret lesen.
            byte[] key = Convert.FromBase64String(configuration["AppSettings:Secret"]);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                // Payload für den JWT.
                Subject = new ClaimsIdentity(new Claim[]
                {
                    // Benutzername als Typ ClaimTypes.Name.
                    new Claim(ClaimTypes.Name, user.username),
                    // Rolle des Benutzer als ClaimTypes.DefaultRoleClaimType setzen.
                    // ACHTUNG: Muss mit den Annotations der Routen in [Authorize(Roles = "xxx")]
                    // übereinstimmen.
                }),
                Expires = DateTime.UtcNow + ExpirationTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            // Den Zeitpunkt des Logins in die DB schreiben.
            return user;
        }

        /// <summary>
        /// Erstellt einen neuen Benutzer in der Datenbank. Dafür wird ein Salt generiert und der
        /// Hash des Passwortes berechnet.
        /// </summary>
        /// <param name="user">Der Benutzer, der in der Datenbank angelegt werden soll.</param>
        /// <returns>Userobjekt, welches in der Datenbank angelegt wurde.</returns>
        public void CreateUser(UserDto user)
        {
            string hash = ComputeSha256Hash(user.password);
            // TODO: 
            // 1. User in die DB schreiben.
            // 2. Das Userobjekt (Modelklasse) statt void zurückgeben.
            if(!db.u_user.Any(x => x.u_username == user.username))
            {
                db.u_user.Add(new u_user
                {
                    u_email = user.email,
                    u_password = hash,
                    u_registerdate = DateTime.Now,
                    u_username = user.username
                });
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Generiert eine Zufallszahl und gibt diese Base64 codiert zurück.
        /// </summary>
        /// <param name="bits">Anzahl der Bits der Zufallszahl.</param>
        /// <returns>Base64 String mit der Länge bits * 4 / 3.</returns>
        private static string GenerateSalt(int bits = 128)
        {
            byte[] salt = new byte[bits / 8];
            // Kryptografisch sichere Zufallszahlen erzeugen.
            using (System.Security.Cryptography.RandomNumberGenerator rnd = System.Security.Cryptography.RandomNumberGenerator.Create())
            {
                rnd.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }

        /// <summary>
        /// Berechnet den HMACSHA256 Wert des übergebenen Passwortes.
        /// </summary>
        /// <param name="password">Passwort, welches codiert werden soll.</param>
        /// <param name="salt">Salt, mit dem der Hashwert berechnet werden soll.</param>
        /// <returns>Base64 codierter String mit 44 Stellen Länge.</returns>
        private static string CalculateHash(string password, string salt)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(salt))
            {
                throw new ArgumentException("Invalid Salt or Passwort.");
            }
            byte[] saltBytes = Convert.FromBase64String(salt);
            byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);

            System.Security.Cryptography.HMACSHA256 myHash = new System.Security.Cryptography.HMACSHA256(saltBytes);

            byte[] hashedData = myHash.ComputeHash(passwordBytes);

            // Das Bytearray wird als Base64 String zurückgegeben.
            string hashedPassword = Convert.ToBase64String(hashedData);
            return hashedPassword;
        }
        static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
