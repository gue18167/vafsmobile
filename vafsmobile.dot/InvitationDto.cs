﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class InvitationDto
    {

        public int user { get; set; }

        public int group { get; set; }

        public DateTime expiredate { get; set; }
    }
}
