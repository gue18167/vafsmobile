﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class GroupDto
    {
        public int id { get; set; }

        public string name { get; set; }

        public int limit { get; set; }

        public string key { get; set; }

        public int? groupstatus { get; set; }
    }
}
