﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class NoteDto
    {
        public int id { get; set; }

        public string description { get; set; }

        public DateTime createdate { get; set; }

        public DateTime deadline { get; set; }

        public int? user { get; set; }

        public int? group { get; set; }

        public string title { get; set; }

        public int? calendarinformation { get; set; }

    }
}
