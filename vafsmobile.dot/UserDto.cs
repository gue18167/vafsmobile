﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class UserDto
    {
        public int id { get; set; }

        public string username { get; set; }

        public string email { get; set; }

        public DateTime registerdate { get; set; }

        public string password { get; set; }
        public string Token { get; set; }
    }
}
