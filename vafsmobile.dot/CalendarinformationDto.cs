﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class CalendarinformationDto
    {
        public int id { get; set; }
        public DateTime createdate { get; set; }

        public DateTime deadline { get; set; }

        public int user { get; set; }

        public int group { get; set; }
    }
}
