﻿using System;
using System.Collections.Generic;

namespace vafsmobile.dto
{
    public partial class GroupmemberDto
    {

        public int user { get; set; }

        public int group { get; set; }

        public int role { get; set; }

        public DateTime membersince { get; set; }
    }
}
