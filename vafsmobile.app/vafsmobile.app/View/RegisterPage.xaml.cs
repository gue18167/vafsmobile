﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private async void RegisterButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                RegisterButton.IsEnabled = false;
                UserDto user = new UserDto()
                {
                    email = this.Email.Text,
                    password = this.Password.Text,
                    registerdate = DateTime.Now,
                    username = this.Username.Text,
                };
                if(await RestService.Instance.TryRegisterAsync(user))
                {
                    Navigation.PushAsync(new LoginPage());
                }
                else
                {
                    await DisplayAlert("Error", "Wrong username or password. Please try again!", "OK");
                }
            }
            catch (Exception err)
            {

            }
            finally
            {
                RegisterButton.IsEnabled = true;
            }
        }
    }
}