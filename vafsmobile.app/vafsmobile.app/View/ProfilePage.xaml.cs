﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
            Username.Text = RestService.Instance.CurrentUser.username;
            Password.Text = "******";
            //TODO Current Profile muss wie in NoteDetail.xaml.cs als Parameter angegeben werden. Außerdem alle Textbox.Text ausfüllen
        }

        private async void Add_ClickedAsync(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddNotesPage());
        }

        private async void CalendarMenuButton_Clicked(object sender, EventArgs e)
        {
            CalendarPageViewModel cpvm = new CalendarPageViewModel(await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance));
            Navigation.PushAsync(new CalendarPage(cpvm));
        }

        private async void NoteMenuButton_Clicked(object sender, EventArgs e)
        {
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new NotesView(new ViewModel.MainPageViewModel(noteRepository, groupRepository)));
        }

        private async void ProfileMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }

        private void GroupMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GroupsView());
        }
    }
}