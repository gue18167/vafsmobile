﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.ViewModel;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoteDetail : ContentPage
    {
        public NoteDetail(NoteDto note)
        {
            InitializeComponent();
            BindingContext = new NoteDetailViewModel(note);

            Title.Text = note.title.ToString();
            Description.Text = note.description.ToString();
            Createdate.Text = note.createdate.ToString();
            Deadline.Text = note.deadline.ToString();
        }

        private async void Close_Button(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}