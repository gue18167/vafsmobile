﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GroupsView : ContentPage
    {
        public GroupsView()
        {
            InitializeComponent();
        }

        private async void PublicGroups_Clicked(object sender, EventArgs e)
        {
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            Navigation.PushAsync(new PublicGroups(new GroupViewModel(groupRepository)));
        }

        private async void MyGroups_Clicked(object sender, EventArgs e)
        {
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            Navigation.PushAsync(new MyGroups(new GroupViewModel(groupRepository)));
        }

        private async void CalendarMenuButton_Clicked(object sender, EventArgs e)
        {
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            Navigation.PushAsync(new CalendarPage(new CalendarPageViewModel(noteRepository)));
        }

        private async void NoteMenuButton_Clicked(object sender, EventArgs e)
        {
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new NotesView(new MainPageViewModel(noteRepository, groupRepository)));
        }

        private async void ProfileMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }
    }
}