﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.View;
using vafsmobile.app.ViewModel;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddNotesPage : ContentPage
    {
        public AddNotesPageViewModel vm;

        public AddNotesPage()
        {
            InitializeComponent();
        }
        public AddNotesPage(AddNotesPageViewModel vm) : this()
        {    
            BindingContext = vm;
            this.vm = vm;
        }

        private async void AddNote_Clicked(object sender, EventArgs e)
        {
            int user = vm.getUser().id;
            NoteDto n = new NoteDto {title = TitleAdd.Text, createdate = DateTime.Now, description = Description.Text, deadline = Deadline.Date, user = vm.getUser().id, group = null, calendarinformation = null };
            await vm.AddNoteAsync(n);
            //vm.notesliste.Add(n);
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new NotesView(new MainPageViewModel(noteRepository, groupRepository)));
            //await Navigation.PushAsync(new MainPage());
        }
    }
}