﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddGroupsPage : ContentPage
    {
        public AddGroupsPage()
        {
            InitializeComponent();
        }

        private GroupViewModel vm;

        public AddGroupsPage(GroupViewModel vm) : this()
        {
            BindingContext = vm;
            this.vm = vm;
        }

        private async void AddGroup_Clicked(object sender, EventArgs e)
        {
            int groupStatusIs = 0;
            if (this.Private.IsChecked) { groupStatusIs = 1; }
            GroupDto group = new GroupDto { name = this.Name.Text, limit = int.Parse(Limit.Text), key = this.Key.Text, groupstatus = groupStatusIs };
            await vm.Add(group);

            Navigation.PushAsync(new MyGroups(vm));
        }
    }
}