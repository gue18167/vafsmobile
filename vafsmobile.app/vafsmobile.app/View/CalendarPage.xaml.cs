﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarPage : ContentPage
    {
        public CalendarPage()
        {
            InitializeComponent();
        }

        private CalendarPageViewModel vm;

        public CalendarPage(CalendarPageViewModel vm) : this()
        {
            BindingContext = vm;
            this.vm = vm;
        }

        //private async void CalendarMenuButton_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new CalendarPage());
        //}

        private async void NoteMenuButton_Clicked(object sender, EventArgs e)
        {
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new NotesView(new MainPageViewModel(noteRepository, groupRepository)));
        }

        private async void ProfileMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }

        private async void Selected_ClickedAsync(object sender, EventArgs e)
        {
            if (Calendar.SelectedItem != null)
            {
                NoteDto note = (NoteDto)Calendar.SelectedItem;
                Navigation.PushAsync(new NoteDetail(note));
            }
            else
            {
                await DisplayAlert("Warning", "Please select a Note", "OK");
            }
        }

        private async void GroupMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GroupsView());
        }
    }
}