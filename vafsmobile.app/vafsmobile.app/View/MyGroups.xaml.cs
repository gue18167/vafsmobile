﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyGroups : ContentPage
    {
        public MyGroups()
        {
            InitializeComponent();
        }

        public MyGroups(GroupViewModel vm) : this()
        {
            BindingContext = vm;
        }

        private async void Create_ClickedAsync(object sender, EventArgs e)
        {
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            Navigation.PushAsync(new AddGroupsPage(new GroupViewModel(groupRepository)));
        }

        private void Delete_ClickedAsnyc(object sender, EventArgs e)
        {

        }

        private void Selected_ClickedAsync(object sender, EventArgs e)
        {

        }

        private async void CalendarMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CalendarPage());
        }

        private async void NoteMenuButton_Clicked(object sender, EventArgs e)
        {
            NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new NotesView(new MainPageViewModel(noteRepository, groupRepository)));
        }

        private async void ProfileMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }

        private async void GroupMenuButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new GroupsView());
        }
    }
}