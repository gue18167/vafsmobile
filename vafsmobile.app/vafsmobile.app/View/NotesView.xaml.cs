﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.ViewModel;
using vafsmobile.dto;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotesView : ContentPage
    {
        private MainPageViewModel vm;
        public NotesView()
        {
            InitializeComponent();
        }

        public NotesView(MainPageViewModel vm) : this()
        {
            BindingContext = vm;
            this.vm = vm;
        }

        private async void Selected_ClickedAsync(object sender, EventArgs e)
        {
            if(Liste.SelectedItem != null)
            {
                NoteDto note = (NoteDto) Liste.SelectedItem;
                Navigation.PushAsync(new NoteDetail(note));
            }
            else
            {
            await DisplayAlert("Warning", "Please select a Note", "OK");
            }
        }

        private async void Delete_ClickedAsnyc(object sender, EventArgs e)
        {
            if(Liste.SelectedItem != null)
            {
                NoteDto note = (NoteDto)Liste.SelectedItem;
                vm.delete(note);
            }
            else
            {
                await DisplayAlert("Warning", "Please select a Note", "OK");
            }
        }

        private async void Add_ClickedAsync(object sender, EventArgs e)
        {
            GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

            Navigation.PushAsync(new AddNotesPage(new AddNotesPageViewModel(vm._noteRepository, groupRepository)));
        }

        private async void CalendarMenuButton_Clicked(object sender, EventArgs e)
        {
            //TODO irgendwas mit repository glaub ich für currentuser
            CalendarPageViewModel cpvm = new CalendarPageViewModel(await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance));
            Navigation.PushAsync(new CalendarPage(cpvm));
        }

        //private async void NoteMenuButton_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new NotesView());
        //}

        private async void ProfileMenuButton_Clicked(object sender, EventArgs e)
        {
            //TODO irgendwas mit repository glaub ich für currentuser
            Navigation.PushAsync(new ProfilePage());
        }

        private async void GroupMenuButton_Clicked(object sender, EventArgs e)
        {
            //TODO irgendwas mit repository glaub ich für currentuser
            Navigation.PushAsync(new GroupsView());
        }
    }
}