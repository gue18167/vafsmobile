﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.app.View;
using vafsmobile.app.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xunit;

namespace vafsmobile.app.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private async void LoginButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                LoginButton.IsEnabled = false;
                // Holt sich das Rest Service als Singleton. Nicht new verwenden,
                // da sonst der Token im Header beim Request nicht mitgesendet
                // wird.
                if (await RestService.Instance.TryLoginAsync(new dto.UserDto
                {
                    username = this.Username.Text,
                    password = this.Password.Text
                }))
                {


                    //    Application.Current.MainPage = new MainPage(
                    //        new NotesView(await MainPageViewModel.FactoryAsync()));
                    NoteRepository noteRepository = await NoteRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);
                    GroupRepository groupRepository = await GroupRepository.CreateAsync(RestService.Instance.CurrentUser, RestService.Instance);

                    Navigation.PushAsync(new NotesView(new MainPageViewModel(noteRepository, groupRepository)));
                }
                else
                {
                    await DisplayAlert("Error", "Wrong username or password. Please try again!", "OK");
                }
               
            }
            catch(Exception err)
            {
               
            }
            finally
            {
                LoginButton.IsEnabled = true;
            }
        }

        private void RegisterButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterPage());
        }
    }
}