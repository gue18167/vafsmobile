﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.dto;

namespace vafsmobile.app.Services
{
    public class NoteRepository
    {
        private readonly RestService _restService;
        private UserDto _user { get; }


        public ObservableCollection<NoteDto> Notes { get; }

        
        private NoteRepository(RestService restService, UserDto user, ObservableCollection<NoteDto> notes)
        {
            _restService = restService;
            _user = user;
            Notes = notes;
        }

        public static async Task<NoteRepository> CreateAsync(UserDto user, RestService restService)
        {
            var notes = await RestService.Instance.SendAsync<ObservableCollection<NoteDto>>(
                HttpMethod.Get, "notes", user.username);

            return new NoteRepository(restService, user, notes);
        }

        public async Task Add(NoteDto note)
        {
            NoteDto newNote = await RestService.Instance.AddNoteAsync(note);
            Notes.Add(newNote);
        }

        public NoteDto CreateNote() => new NoteDto { user = _user.id };

        public UserDto getUser()
        {
            return _restService.getCurrentUser();
        }

        public async Task Remove (NoteDto note)
        {
            await RestService.Instance.DeleteNote(note.id.ToString());
            Notes.Remove(note);
        }
    }
}
