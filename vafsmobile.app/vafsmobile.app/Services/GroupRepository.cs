﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using vafsmobile.dto;

namespace vafsmobile.app.Services
{
    public class GroupRepository
    {
        private readonly RestService _restService;
        private UserDto _user { get; }

        public ObservableCollection<GroupDto> Groups { get; }

        public ObservableCollection<GroupDto> MyGroups { get; }

        private GroupRepository(RestService restService, UserDto user, ObservableCollection<GroupDto> groups, ObservableCollection<GroupDto> myGroups)
        {
            _restService = restService;
            _user = user;
            Groups = groups;
            MyGroups = myGroups;
        }

        public static async Task<GroupRepository> CreateAsync(UserDto user, RestService restService)
        {
            var groups = await RestService.Instance.SendAsync<ObservableCollection<GroupDto>>(
                HttpMethod.Get, "groups", user.username);

            var myGroups = await RestService.Instance.SendAsync<ObservableCollection<GroupDto>>(
                HttpMethod.Get, "groups/mygroups", user.username);

            return new GroupRepository(restService, user, groups, myGroups);
        }

        public async Task<GroupDto> Add(GroupDto group)
        {
            GroupDto group1 = await RestService.Instance.AddGroupAsync(group);
            Groups.Add(group1);
            return group1;
        }

        public async Task Remove(GroupDto group)
        {
            await RestService.Instance.DeleteGroup(group.id.ToString());
            Groups.Remove(group);
        }

        public async Task JoinGroup(GroupDto group1)
        {
            GroupmemberDto groupmemberDto = new GroupmemberDto() { group = group1.id, membersince = DateTime.Now, user = _user.id, role = 2 };

            var group = await RestService.Instance.SendAsync<GroupDto>(HttpMethod.Post, "groups", groupmemberDto);

            Groups.Remove(group);
        }
    }
}
