﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.dto;

namespace vafsmobile.app.ViewModel
{
    public class GroupViewModel : BaseViewModel
    {
        private readonly GroupRepository _groupRepository;

        public ObservableCollection<GroupDto> Groups => _groupRepository?.Groups;

        public ObservableCollection<GroupDto> MyGroups => _groupRepository?.MyGroups;

        public GroupDto SelectedGroup { get; set; }

        public GroupDto SelectedMyGroup { get; set; }

        public GroupViewModel(GroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public async Task delete(GroupDto group)
        {
            _groupRepository.Remove(group);
            Groups.Remove(group);
        }

        public async Task JoinGroupe()
        {
            if(SelectedGroup != null)
            {
                await _groupRepository.JoinGroup(SelectedGroup);
                Groups.Remove(SelectedGroup);
            }
        }

        public async Task Add(GroupDto group)
        {
            Groups.Add(await _groupRepository.Add(group));
        }
    }
}
