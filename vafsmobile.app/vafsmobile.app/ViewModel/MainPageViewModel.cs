﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using vafsmobile.app.Services;
using vafsmobile.dto;
using Xamarin.Forms;

namespace vafsmobile.app.ViewModel
{
    public class MainPageViewModel : BaseViewModel
    {
        public readonly NoteRepository _noteRepository;

        public readonly GroupRepository _groupRepository;

        private GroupDto selectedGroup;

        public GroupDto SelectedGroup
        {
            get
            {
                return selectedGroup;
            }
            set
            {
                selectedGroup = value;
                OnNotifyPropertyChanged("Notes");
            }
        }

        public ObservableCollection<NoteDto> AllNotes => _noteRepository?.Notes;

        public ObservableCollection<GroupDto> MyGroups => _groupRepository?.MyGroups;

        private ObservableCollection<NoteDto> notes;

        public ObservableCollection<NoteDto> Notes
        {
            get
            {
                ObservableCollection<NoteDto> result = new ObservableCollection<NoteDto>();
                if (SelectedGroup != null)
                {
                    result = new ObservableCollection<NoteDto>(AllNotes.Where(x => x.group == SelectedGroup.id));
                }
                else
                {
                    result = new ObservableCollection<NoteDto>(AllNotes);
                }
                return result;
            }
            set
            {
                notes = value;
            }
        }

        public MainPageViewModel(NoteRepository noteRepository, GroupRepository groupRepository)
        {
            _noteRepository = noteRepository;
            _groupRepository = groupRepository;
        }

        public async Task delete(NoteDto note)
        {
            _noteRepository.Remove(note);
            AllNotes.Remove(note);
        }
    }
}
