﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using vafsmobile.app.Services;
using vafsmobile.dto;
using Xamarin.Forms.Internals;

namespace vafsmobile.app.ViewModel
{
    public class CalendarPageViewModel : BaseViewModel
    {
        private NoteRepository noteRepository { get; set; }

        private ObservableCollection<NoteDto> notesByDate;
        public ObservableCollection<NoteDto> NotesByDate {
            get => notesByDate;
            set {
                notesByDate = value;
                OnNotifyPropertyChanged("NotesByDate");
            }
        }
        private DateTime _selectedDate;
        public DateTime SelectedDate {
            get => _selectedDate;
            set {
                _selectedDate = value;
                NotesByDate = new ObservableCollection<NoteDto>(noteRepository.Notes.Where(x => x.deadline.Date == _selectedDate.Date));
            }
        }

        public CalendarPageViewModel(NoteRepository noteRepository)
        {
            this.noteRepository = noteRepository;
            SelectedDate = DateTime.Now;
        }
    }
}
