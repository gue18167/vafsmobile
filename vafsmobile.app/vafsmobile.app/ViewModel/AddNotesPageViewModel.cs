﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using vafsmobile.app.Services;
using vafsmobile.dto;

namespace vafsmobile.app.ViewModel
{
    public class AddNotesPageViewModel : BaseViewModel
    {
        private readonly NoteRepository _NoteRepository;

        private readonly GroupRepository _groupRepository;

        public ObservableCollection<GroupDto> MyGroups => _groupRepository?.MyGroups;

        public GroupDto SelectedGroup { get; set; }

        private UserDto currentUser;

        public NoteDto Note { get; set; }
       
        public AddNotesPageViewModel(NoteRepository noteRepository, GroupRepository groupRepository)
        {
            currentUser = noteRepository.getUser();
            _NoteRepository = noteRepository;
            _groupRepository = groupRepository;
        }

        public async Task AddNoteAsync(NoteDto note)
        {
            Note = note ?? _NoteRepository.CreateNote();
            if (SelectedGroup != null)
            {
                Note.group = SelectedGroup.id;
            }   
            await _NoteRepository.Add(Note);
        }
        //public static async Task<AddNotesPageViewModel> FactoryAsync()
        //{
            
        //    AddNotesPageViewModel vm = new AddNotesPageViewModel();
        //    return vm;
        //}

        public UserDto getUser()
        {
            return currentUser;
        }
    }
}
